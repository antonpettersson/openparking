import { CardComponent } from './../card/card.component';
import { Component, OnInit } from '@angular/core';
import { HandleParkingsService } from '../services/handle-parkings.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  providers: [CardComponent]
})
export class MenuComponent implements OnInit {
  isVisible = false;
  constructor(private handleParkingService: HandleParkingsService,
  private cardComponent: CardComponent) { }

  ngOnInit() {
  }

  public refreshParkingPlots() {
    this.cardComponent.fetchPlots();
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isVisible = false;
  }
}
