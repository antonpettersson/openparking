import { TestBed, inject } from '@angular/core/testing';

import { HandleGeolocationService } from './handle-geolocation.service';

describe('HandleGeolocationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HandleGeolocationService]
    });
  });

  it('should be created', inject([HandleGeolocationService], (service: HandleGeolocationService) => {
    expect(service).toBeTruthy();
  }));
});
