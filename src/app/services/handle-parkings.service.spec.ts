import { TestBed, inject } from '@angular/core/testing';

import { HandleParkingsService } from './handle-parkings.service';

describe('HandleParkingsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HandleParkingsService]
    });
  });

  it('should be created', inject([HandleParkingsService], (service: HandleParkingsService) => {
    expect(service).toBeTruthy();
  }));
});
