import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HandleGeolocationService {

  constructor() { }

  options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

  public getGeolocation() {
    if (navigator.geolocation) {
      // if geo-location is accepted
      navigator.geolocation.getCurrentPosition(this.success, this.failure, this.options);
    } else {
      // else sadface
      console.log('Geolocation has been denied/disabled. ');
    }
  }

  success(pos) {
    // handle success-scenario
    console.log('pos: ', pos);
  }

  failure(pos) {
    // handle fail-scenario
    console.log('sadface: ', pos);
  }
}
