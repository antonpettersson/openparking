import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable, interval } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

export interface ParkingPlot {
  Id: string;
  Name: string;
  TotalParkingSpaces?: number;
  AvailableParkingSpaces?: number;
  AvailableParkingSpacesDate?: string;
  PhoneParkingCode?: string;
}

export interface ParkingPlots {
  plots: ParkingPlot[];
}

@Injectable({
  providedIn: 'root'
})
export class HandleParkingsService {
  plots = [];
  privateTollParkingsUrl = 'v2.1/privatetollparkings/1cebedca-029d-4de6-8b0e-62a30cca9071?&format=json';

  constructor(private http: HttpClient) { }

  private getOpenParkings() {
    return this.http.get(environment.baseUrl + this.privateTollParkingsUrl);
  }

  public handleOpenParkings(): any {
    this.plots = []; // making sure it's empty
    this.getOpenParkings().subscribe(
      (plots: any[]) => {
        // console.log('PLOTS RAW: ', plots);
        if (!isNullOrUndefined(plots)) {
          for (let i = 0; i < plots.length; i++) {
            if (!isNullOrUndefined(plots[i].ParkingSpaces) && !isNullOrUndefined(plots[i].FreeSpaces) &&
              (plots[i].ParkingSpaces > plots[i].FreeSpaces)) {
              this.plots.push(plots[i]);
            }
          }
        }
        return this.plots;
      });
    return this.plots;
  }
}
