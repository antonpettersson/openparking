import { HandleGeolocationService } from './../services/handle-geolocation.service';
import { isNullOrUndefined } from 'util';
import { HandleParkingsService } from './../services/handle-parkings.service';
import { Component, OnInit, OnChanges, SimpleChanges, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit, OnChanges {
  plots = [];
  constructor(private handleParkingsService: HandleParkingsService,
    private handleGeolocationService: HandleGeolocationService) { }

  ngOnInit() {
    this.fetchPlots();
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes: ', changes);
  }

  public fetchPlots() {
    this.plots = this.handleParkingsService.handleOpenParkings();
  }

  printParkingPlotsArray() {
    console.log('printParkingPltos: ', this.plots);
  }

  openWayPoint() {
    this.handleGeolocationService.getGeolocation();
  }
}
